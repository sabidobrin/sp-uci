(function() {
    /* global angular */
    function komentarModalnoOkno($uibModalInstance, podrobnostiLokacije) {
        var vm = this;
        
        vm.podrobnostiLokacije = podrobnostiLokacije;
        
        vm.modalnoOkno = {
            zapri: function odgovor() {
                $uibModalInstance.close(odgovor);
            },
            preklici: function() {
                $uibModalInstance.close();
            }
        };
        
        vm.dodajKomentar = function(idLokacije, podatkiObrazca) {
            edugeocachePodatki.dodajKomentarZaId(idLokacije, {
                ocena: podatkiObrazca.ocena,
                komentar: podatkiObrazca.komentar
            }).then(
                function success(odgovor) {
                    console.log("uspesno dodajanje komentarja");
                    vm.modalnoOkno.zapri(odgovor.data);
                },
                function error(odgovor) {
                    vm.napakaNaObrazcu = "napaka pri shranjevanju komentarja, poskusite znova";
                });
            return false;
        };

    vm.posiljanjePodatkov = function() {
        vm.napakaNaObrazcu = "";
        if(!vm.podatkiObrazca.ocena || !vm.podatkiObrazca.komentar) {
            vm.napakaNaObrazcu = "izpolnite vsa vnosna polja";
            return false;
        }
        else {
            vm.dodajKomentar(vm.podrobnostiLokacije.idLokacije, vm.podatkiObrazca);
        }
    };
};
    
    komentarModalnoOkno.$inject = ['$uibModalInstance', 'podrobnostiLokacije'];
    
    angular
        .module('edugeocache')
        .controller('komentarModalnoOkno', komentarModalnoOkno);
})();