(function() {
    /* global angular */
    
    function podrobnostiLokacijeCtrl($routeParams, $uibModal, edugeocachePodatki, avtentikacija) {
        var vm = this;
        vm.idLokacije = $routeParams.idLokacije;
        
        vm.jePrijavljen = avtentikacija.jePrijavljen();
        vm.prvotnaStran = $location.path();
        
        edugeocachePodatki.podrobnostiLokacijeZaId(vm.idLokacije).then(
            function success(odgovor) {
                vm.podatki = { lokacija: odgovor.data };
                vm.glavaStrani = {
                    naslov: vm.podatki.lokacija.naziv
                };
            },
            function error(odgovor) {
                console.log(odgovor.e);
            });
    
    vm.prikaziPojavnoOknoObrazca = function() {
        var primerekModalnegaOkna = $uibModal.open({
            templateUrl: '/komentarModalnoOkno/komentarModalnoOkno.pogled.html',
            controller: 'komentarModalnoOkno',
            controllerAs: 'vm',
            resolve: {
                podrobnostiLokacije: function() {
                    return {
                        idLokacije: vm.idLokacije,
                        nazivLokacije: vm.podatki.lokacija.naziv
                    };
                }
            }
        });

        primerekModalnegaOkna.result.then(function(podatki) {
            if(typeof podatki != 'undefined') {
                vm.podatki.lokacija.komentarji.push(podatki);
            }
        });
    };
    
    podrobnostiLokacijeCtrl.$inject=['$routeParams', '$location', '$uibModal', 'edugeocachePodatki'];
    
    angular
        .module('edugeocache')
        .controller('podrobnostiLokacijeCtrl', podrobnostiLokacijeCtrl);
    };

})();