(function() {

/* global angular */
angular
    .module('edugeocache')
    .controller('seznamCtrl', seznamCtrl);

seznamCtrl.$inject = ['$scope', 'edugeocachePodatki', 'geolokacija'];
function seznamCtrl($scope, edugeocachePodatki, geolokacija) {
    var vm = this;
    vm.glavaStrani = {
        naslov: 'EduGeoCache',
        podnaslov: 'poiscite zanimive lokacije blizu vas'
    };
    vm.stranskaOrodnaVrstica = 'iscete lokacijo za kratkocasenje? juhu'
    vm.sporocilo = 'pridobivam trenutno lokacijo ...'
    
    vm.pridobiPodatke = function(lokacija) {
        var lat = lokacija.coords.latitude,
            lng = lokacija.coords.longitude;
        vm.sporocilo = "iscem bliznje lokacije ...";
        edugeocachePodatki.koordinateTrenutneLokacije(lat, lng).then(
            function success(odgovor) {
                vm.sporocilo = odgovor.data.length > 0 ? "" : "ne najdem lokacij";
                vm.data = { lokacije: odgovor.data };
            },
            function error(odgovor) {
                vm.sporocilo = "prislo je do napake";
                console.log(odgovor.e);
            });
    };
    
    var prikaziNapako = function(napaka) {
        $scope.$apply(function() {
            vm.sporocilo = napaka.message;
        });
    };
    
    vm.niLokacije = function() {
        $scope.$apply(function() {
            vm.sporocilo = "vas brskalnik ne podpira geolociranja";
        });
    };
    
    geolokacija.vrniLokacijo(vm.pridobiPodatke, vm.prikaziNapako, vm.niLokacije);
}
})();