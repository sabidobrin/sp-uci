(function() {
/* global angular
    global navigator */
    
var geolokacije = function() {
    var vrniLokacijo = function(pkUspesno, pkNapaka, pkNiLokacije) {
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(pkUspesno, pkNapaka);
            }
            else {
                pkNiLokacije();
            }
    };
    return {
        vrniLokacijo: vrniLokacijo
    };
};

angular
    .module('edugeocache')
    .service('geolokacija', geolokacija);
})();