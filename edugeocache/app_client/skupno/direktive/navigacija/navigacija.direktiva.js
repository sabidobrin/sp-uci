(function() {
    /* global angular */
    var navigacija = function() {
        return {
            restrict: 'EA',
            templateUrl: "/skupno/direktive/navigacija/navigacija.predloga.html",
            controller: 'navigacijaCtrl',
            controllerAs: 'navvm'
        };
    };
    
    angular
        .module('edugeocache')
        .directive('navigacija', navigacija);
})();