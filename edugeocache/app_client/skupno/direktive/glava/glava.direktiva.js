(function() {
    /* global angular */
    var glava = function() {
        return {
            restrict: 'EA',
            scope: {
                vsebina: '=vsebina'
            },
            templateUrl: "/skupno/direktive/glava/glava.predloga.html"
        };
    };
    
    angular
        .module('edugeocache')
        .directive('glava', glava);
})();