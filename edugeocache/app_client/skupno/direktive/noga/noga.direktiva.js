(function() {
    /* global angular */
    var noga = function() {
        return {
            restrict: 'EA',
            templateUrl: "/skupno/direktive/noga/noga.predloga.html"
        };
    };
    
    angular
        .module('edugeocache')
        .directive('noga', noga);
})();