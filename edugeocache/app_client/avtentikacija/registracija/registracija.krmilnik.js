(function() {
    /* global angular */
    
    registracijaCtrl.$inject = ['$location', 'avtentikacija'];
  function registracijaCtrl($location, avtentikacija) {
    var vm = this;
    
    vm.glavaStrani = {
      naslov: 'Kreiranje novega EduGeoCache uporabniškega računa'
    };
    
    vm.prijavniPodatki = {
      ime: "",
      elektronskiNaslov: "",
      geslo: ""
    };
    
    vm.prvotnaStran = $location.search().stran || '/';
    
    vm.posiljanjePodatkov = function() {
        vm.napakaNaObrazcu = "";
        if (!vm.prijavniPodatki.ime || !vm.prijavniPodatki.elektronskiNaslov || !vm.prijavniPodatki.geslo) {
            vm.napakaNaObrazcu = "Zahtevani so vsi podatki, prosim poskusite znova!";
            return false;
        } else {
        vm.izvediRegistracijo();
    };
  }
  
  angular
    .module('edugeocache')
    .controller('registracijaCtrl', registracijaCtrl);
})();