(function() {
    /* global angular */
    function informacijeCtrl() {
        var vm = this;
        vm.glavaStrani = {
            naslov: 'Informacije o aplikaciji'
        };
        vm.osrednjiDel = {
            vsebina: 'EduGeoCache se uporablja za iskanje zanimivih lokacij ipd'
        }
    };
    
    angular
        .module('edugeocache')
        .controller('informacijeCtrl', informacijeCtrl);
})