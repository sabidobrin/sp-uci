var passport = require('passport');
var LokalnaStrategija = require('passport-local').Strategy;
var mongoose = require('mongoose');
var uporabnik = mongoose.model('Uporabnik');

passport.use(new LokalnaStrategija({
    usenameField: 'elektronskiNaslov',
    passwordField: 'geslo'
},
function(uporabniskoIme, geslo, koncano) {
    Uporabnik.findOne(
        { elektronskiNaslov: uporabniskoIme },
        function(napaka, uporabnik) {
            if(napaka) { return koncano(napaka); }
            if(!uporabnik) {
                return koncano(null, false, {
                    sporocilo: 'napacno uporabnisko ime'
                });
            }
            if(!uporabnik.preveriGeslo(geslo)) {
                return koncano(null, false, {
                    sporocilo: 'napacno geslo'
                });
            }
            return koncano(null, uporabnik);
        })
    }
));