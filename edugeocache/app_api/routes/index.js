var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var avtentikacija = jwt ({
    secret: process.enc.JWT_geslo,
    userProperty: 'payload'
});

var ctrlLokacije = require('../controllers/lokacije');
var ctrlKomentarji = require('../controllers/komentarji');
var ctrlAvtentikacija = require('../controllers/avtentikacija');

// Lokacije
router.get('/lokacije', ctrlLokacije.lokacijeSeznamPoRazdalji);
router.post('/lokacije', ctrlLokacije.lokacijeKreiraj);
router.get('/lokacije/:idLokacije', ctrlLokacije.lokacijePreberiIzbrano);
router.put('/lokacije/:idLokacije', ctrlLokacije.lokacijePosodobiIzbrano);
router.delete('/lokacije/:idLokacije', ctrlLokacije.lokacijeIzbrisiIzbrano);

// Komentarji
router.post('/lokacije/:idLokacije/komentarji', avtentikacija, ctrlKomentarji.komentarjiKreiraj);
router.get('/lokacije/:idLokacije/komentarji/:idKomentarja', avtentikacija, ctrlKomentarji.komentarjiPreberiIzbranega);
router.put('/lokacije/:idLokacije/komentarji/:idKomentarja', avtentikacija, ctrlKomentarji.komentarjiPosodobiIzbranega);
router.delete('/lokacije/:idLokacije/komentarji/:idKomentarja', avtentikacija, ctrlKomentarji.komentarjiIzbrisiIzbranega);

module.exports = router;

// avtentikacija
router.post('/registracija', ctrlAvtentikacija.registracija);
router.post('/prijava', ctrlAvtentikacija.prijava);
