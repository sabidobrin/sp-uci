var mongoose = require('mongoose');
var Lokacija = mongoose.model('Lokacija');
var Uporabnik = mongoose.model('Uporabnik');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
    odgovor.status(status);
    odgovor.json(vsebina);
};

module.exports.komentarjiKreiraj = function(zahteva, odgovor) {
    vrniAvtorja(zahteva, odgovor, function(zahteva, odgovor, imeUporabnika) {
        var idLokacije = zahteva.params.idLokacije;
        if(idLokacije) {
            Lokacija
                .findById(idLokacije)
                .select('komentarji')
                .exec(
                    function(napaka, lokacija) {
                        if(napaka) {
                            vrniJsonOdgovor(odgovor, 400, napaka);
                        }
                        else {
                            dodajKomentar(zahteva, odgovor, lokacija, imeUporabnika);
                        }
                    }
                );
        } else {
            vrniJsonOdgovor(odgovor, 404, { "sporočilo": "ne najdem lokacije, idLokacije je obvezen parameter "}); 
        }
    });
};

module.exports.komentarjiPreberiIzbranega = function(zahteva, odgovor) {
    if(zahteva.params && zahteva.params.idLokacije && zahteva.params.idKomentarja) {
        Lokacija
            .findById(zahteva.params.idLokacije)
            .select('naziv komentarji')
            .exec(
                function(napaka, lokacija) {
                    var rezultat, komentar;
                    if(!lokacija) {
                        vrniJsonOdgovor(odgovor, 404, {"sporočilo": "ne najdem lokacije s podanim enoličnim identifikatorjem idLokacije" });
                        return;
                    }
                    else if (napaka) {
                        vrniJsonOdgovor(odgovor, 404, napaka);
                        return;
                    }
                    if (lokacija.komentarji && lokacija.komentarji.length > 0) {
                        komentar = lokacija.komentarji.id(zahteva.params.idKomentarja);
                        if(!komentar) {
                            vrniJsonOdgovor(odgovor, 404, { "sporočilo": "ne najdem komentarja z enoličnim identifikatorjem idKomentarja" });
                        } else {
                            rezultat = {
                                lokacija: { naziv: lokacija.naziv, id: zahteva.params.idLokacije },
                                komentar: komentar
                            };
                            vrniJsonOdgovor(odgovor, 200, rezultat);
                        }
                    } else {
                        vrniJsonOdgovor(odgovor, 404, {"sporočilo": "ne najdem nobenega komentarja" });
                    }
                }
            );
    } else {
            vrniJsonOdgovor(odgovor, 404, { "sporočilo": "ne najdem zapisa" });
    }
};

module.exports.komentarjiPosodobiIzbranega = function(zahteva, odgovor) {
    if(!zahteva.params.idLokacije || !zahteva.params.idKomentarja) {
        vrniJsonOdgovor(odgovor, 404, { "sporočilo": "ne najdem lokacije oz. komentarja" });
        return;
    }
    Lokacija
        .findById(zahteva.params.idLokacije)
        .select('komentarji')
        .exec(
            function(napaka, lokacija) {
                var trenutniKomentar;
                if(!lokacija) {
                    vrniJsonOdgovor(odgovor, 404, {"sporočilo": "lokacije ne najdem" });
                    return;
                }
                else if (napaka) {
                    vrniJsonOdgovor(odgovor, 400, napaka);
                    return;
                }
                if(lokacija.komentarji && lokacija.komentarji.length > 0) {
                    trenutniKomentar = lokacija.komentarji.id(zahteva.params.idKomentarja);
                    if(!trenutniKomentar) {
                        vrniJsonOdgovor(odgovor, 404, { "sporočilo": "komentarja ne najdem" });
                    }
                    else {
                        trenutniKomentar.avtor = zahteva.body.avtor;
                        trenutniKomentar.ocena = zahteva.body.ocena;
                        trenutniKomentar.besediloKomentarja = zahteva.body.besediloKomentarja;
                        lokacija.save(function(napaka, lokacija) {
                            if(napaka) {
                                vrniJsonOdgovor(odgovor, 404, napaka);
                            }
                            else {
                                posodobiPovprecnoOceno(lokacija._id);
                                vrniJsonOdgovor(odgovor, 200, trenutniKomentar);
                            }
                        });
                    }
                    
                }
                else {
                    vrniJsonOdgovor(odgovor, 404, { "sporočilo": "ni komentarjev za ažuriranje "});
                }
            });
};

module.exports.komentarjiIzbrisiIzbranega = function(zahteva, odgovor) {
    if(!zahteva.params.idLokacije || !zahteva.params.idKomentarja) {
        vrniJsonOdgovor(odgovor, 404, { "sporočilo": "ne najdem lokacije oz. komentarja" });
        return;
    }
    Lokacija
        .findById(zahteva.params.idLokacije)
        .exec(
            function(napaka, lokacija) {
                if(!lokacija) {
                    vrniJsonOdgovor(odgovor, 404, { "sporočilo": "lokacije ne najdem" });
                    return;
                }
                else if (napaka) {
                    vrniJsonOdgovor(odgovor, 404, napaka);
                    return;
                }
                if(lokacija.komentarji && lokacija.komentarji.length > 0) {
                    if(!lokacija.komentarji.id(zahteva.params.idKomentarja)) {
                        vrniJsonOdgovor(odgovor, 404, { "sporočilo": "komentarja ne najdem" });
                    }
                    else {
                        lokacija.komentarji.id(zahteva.params.idKomentarja).remove();
                        lokacija.save(function(napaka) {
                            if (napaka) {
                                vrniJsonOdgovor(odgovor, 404, napaka);
                            }
                            else {
                                posodobiPovprecnoOceno(lokacija._id);
                                vrniJsonOdgovor(odgovor, 204, null);
                            }
                        });
                    }
                }
                else {
                    vrniJsonOdgovor(odgovor, 404, { "sporočilo": "ni komentarja za brisanje" });
                }
            });
};

var dodajKomentar = function(zahteva, odgovor, lokacija, avtor) {
    if(!lokacija) {
        vrniJsonOdgovor(odgovor, 404, { "sporočilo": "ne najdem lokacije "});
    }
    else {
        lokacija.komentarji.push({
            avtor: avtor,
            ocena: zahteva.body.ocena,
            besediloKomentarja: zahteva.body.komentar
        });
        lokacija.save(function(napaka, lokacija) {
            var dodaniKomentar;
            if(napaka) {
                console.log(napaka);
                vrniJsonOdgovor(odgovor, 400, napaka);
            }
            else {
                posodobiPovprecnoOceno(lokacija._id);
                dodaniKomentar = lokacija.komentarji[lokacija.komentarji.length-1];
                vrniJsonOdgovor(odgovor, 201, dodaniKomentar);
            }
        });
    }
};

var posodobiPovprecnoOceno = function(idLokacije) {
    Lokacija
        .findById(idLokacije)
        .select('ocena komentarji')
        .exec(
            function(napaka, lokacija) {
                if(!napaka) {
                    izracunajPovprecnoOceno(lokacija);
                }
            });
};

var izracunajPovprecnoOceno = function (lokacija) {
    var stevec, steviloKomentarjev, povprecnaOcena, skupnaOcena;
    if(lokacija.komentarji && lokacija.komentarji.length > 0) {
        steviloKomentarjev = lokacija.komentarji.length;
        skupnaOcena = 0;
        for (stevec = 0; stevec < steviloKomentarjev; stevec++) {
            skupnaOcena += lokacija.komentarji[stevec].ocena;
        }
        povprecnaOcena = parseInt(skupnaOcena / steviloKomentarjev, 10);
        lokacija.ocena = povprecnaOcena;
        lokacija.save(function(napaka) {
            if(napaka) {
                console.log(napaka);
            }
            else {
                console.log("povprecna ocena je posodobljena na " + povprecnaOcena);
            }
        });
    }
};

var vrniAvtorja = function(zahteva, odgovor, povratniKlic) {
  if(zahteva.payload && zahteva.payload.elektronskiNaslov) {
      Uporabnik
        .findOne({ elektronskiNaslov: zahteva.payload.elektronskiNaslov })
        .exec(function(napaka, uporabnik) {
            if(!uporabnik) {
                vrniJsonOdgovor(odgovor, 404, {
                    "sporocilo": "ne najdem uporabnika"
                });
                return;
            }
            else if (napaka) {
                console.log(napaka);
                vrniJsonOdgovor(odgovor, 404, napaka);
                return;
            }
            povratniKlic(zahteva, odgovor, uporabnik.ime);
        });
  }
  else {
      vrniJsonOdgovor(odgovor, 404, {
          "sporocilo": "ne najdem uporabnika"
      });
      return;
  }
};