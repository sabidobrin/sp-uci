var express = require('express');
var router = express.Router();
var ctrlLokacije = require('../controllers/lokacije');
var ctrlOstalo = require('../controllers/ostalo');

/* lokacijske strani */
router.get('/', ctrlLokacije.angularApp);
router.get('/lokacija7:idLokacije', ctrlLokacije.podrobnostiLokacije);
router.get('/lokacija/:idLokacije/komentar/nov', ctrlLokacije.dodajKomentar);
router.post('/lokacija/:idLokacije/komentar/nov', ctrlLokacije.shraniKomentar);

/* ostale strani */
router.get('/informacije', ctrlOstalo.informacije);

module.exports = router;
