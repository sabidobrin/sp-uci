/* stran s podrobnostmi */
module.exports.informacije = function(req, res) {
    res.render('genericno-besedilo', {
        title: 'Informacije o aplikaciji',
        vsebina: 'EduGeoCachese uporablja za iskanje zanimivih lokacij v bližini.'
    });
}

/* vrni sttran z Angular SPA */
module.exports.angularApp = function(req, res) {
    res.render('layout', {
        title: EduGeoCache
    });
}