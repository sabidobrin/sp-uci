var request = require('request');
var apiParametri = {
    streznik: "http://localhost:" + process.env.PORT
};
if(process.env.NODE_ENV === 'production') {
    apiParametri.streznik = "https://edugeocache.herokuapp.com";
}

var prikaziZacetniSeznam = function(zahteva, odgovor) {
    odgovor.render('lokacije-seznam', {
        title: 'Seznam lokacij',
        glavaStrani: {
            naslov: 'EduGeoCache',
            podnaslov: 'Zanimive lokacije blizu vas!'
        },
        stranskaOrodnaVrstica: 'Iščete lokacije za kratkočasenje? Ste na pravem mestu!',
    });
};

var prikaziPodrobnostiLokacije = function(zahteva, odgovor, podrobnostiLokacije) {
    odgovor.render('lokacija-podrobnosti', {
        title: podrobnostiLokacije.naziv,
        glavaStrani: {
            naslov: podrobnostiLokacije.naziv
        },
        stranskaOrodnaVrstica: {
            kontekst: ' je na EduGeoCache zanimiva lokacija, priporočena s strani uporabnikov.',
            poziv: 'Napišite vaše mnenje!'
        },
        lokacija: podrobnostiLokacije
    });
};

/* seznam lokacij */
module.exports.seznam = function(req, res) {
};

/* podrobnosti */
module.exports.podrobnostiLokacije = function(req, res) {
    pridobiPodrobnostiLokacije(req, res, function(zahteva, odgovor, vsebina) {
        prikaziPodrobnostiLokacije(zahteva, odgovor, vsebina);
    });
};

/* dodajanje komentarja */
module.exports.dodajKomentar = function(req, res) {
    pridobiPodrobnostiLokacije(req, res, function(zahteva, odgovor, vsebina) {
        prikaziObrazecZaKomentar(zahteva, odgovor, vsebina);
    });
};

var formatirajRazdaljo = function(razdalja) {
    var vrednostRazdalje, enota;
    if(razdalja > 1) {
        vrednostRazdalje = parseFloat(razdalja).toFixed(1);
        enota = ' km';
    }
    else {
        vrednostRazdalje = parseInt(razdalja * 1000, 10);
        enota = ' m';
    }
    return vrednostRazdalje + enota;
};

var prikaziNapako = function(zahteva, odgovor, kodaStatusa) {
    var naslov, vsebina;
    if(kodaStatusa == 404) {
        naslov = "404, strani ni mogoce najti";
        vsebina = "nekaj je slo narobe";
    }
    else {
        naslov = kodaStatusa + ", nekaj je slo narobe";
        vsebina = "ocitno nekje nekaj ne deluje";
    }
    odgovor.status(kodaStatusa);
    odgovor.render('genericno-besedilo', {
        title: naslov,
        vsebina: vsebina
    });
};

// shrani komentar na strežnik
module.exports.shraniKomentar = function(req, res) {
    var parametriZahteve, pot, idLokacije, posredovaniPodatki;
    idLokacije = req.params.idLokacije;
    pot = '/api/lokacije/' + idLokacije + '/komentarji';
    posredovaniPodatki = {
        naziv: req.body.naziv,
        ocena: req.body.ocena,
        komentar: req.body.komentar
    };
    parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'POST',
        json: posredovaniPodatki
    };
    if(!posredovaniPodatki.naziv || !posredovaniPodatki.ocena || !posredovaniPodatki.komentar) {
        res.redirect('/lokacija/' + idLokacije + '/komentar/nov?napaka=vrednost');
    }
    else {
        request(
            parametriZahteve,
            function(napaka, odgovor, vsebina) {
                if(odgovor.statusCode == 201) {
                    res.redirect('/lokacija/' + idLokacije);
                }
                else if (odgovor.statusCode == 400 && vsebina.name && vsebina.name =='ValidationError') {
                    res.redirect('/lokacija/' + idLokacije + '/komentar/nov?napaka=vrednost');
                }
                else {
                    prikaziNapako(req, res, odgovor.statusCode);
                }
            });
    }
};

var prikaziObrazecZaKomentar = function(zahteva, odgovor, podrobnostiLokacije) {
    odgovor.render('lokacija-nov-komentar', {
        title: 'Dodaj komentar za ' + podrobnostiLokacije.naziv,
        glavaStrani: {
            naslov: 'Komentiraj ' + podrobnostiLokacije.naziv
        },
        napaka: zahteva.query.napaka,
        url: zahteva.originalUrl
    });
};

var pridobiPodrobnostiLokacije = function(req, res, callback) {
    var parametriZahteve, pot;
    pot = '/api/lokacije' + req.params.idLokacije;
    parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'GET',
        json: {}
    };
    request(
        parametriZahteve,
        function(napaka, odgovor, vsebina) {
            if(odgovor.statusCode == 200) {
                var podatki = vsebina;
                podatki.koordinate = {
                    lng: vsebina.koordinate[0],
                    lat: vsebina.koordinate[1]
                };
                callback(req, res, podatki);
            }
            else {
                prikaziNapako(req, res, odgovor.statusCode);
            }
        });
};